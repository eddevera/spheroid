﻿using UnityEngine;
using System.Collections;

// Cast a ray form some origin to the center of the screen @ infinity and return the direction of the cast.
public class PlayerRaycaster : MonoBehaviour 
{
	public Vector3 CastRay(int damage)
	{
		if(Time.timeScale != 0)
		{
			Transform origin = GameObject.Find("Player").GetComponentInChildren<Camera>().transform;
			Ray forward = new Ray(origin.position, origin.forward);
			RaycastHit hit = new RaycastHit();

			int layerMask = 1 << 9;
			if(Physics.Raycast(forward,out hit,Mathf.Infinity,~layerMask))
			{
				Hittable tmp = hit.collider.gameObject.GetComponent<Hittable>();
				if(tmp != null)
				{
					tmp.OnHit(forward.direction, damage);
				}
			}
			return forward.direction; // Always return the forward direction.
		}
		return Vector3.zero;
	}
}
