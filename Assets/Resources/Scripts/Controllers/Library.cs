﻿using UnityEngine;
using System.Collections;

// Some global stuff I needed/didn't feel like typing repeatedly.

public static class Library
{
	public const int layerEnemy = 10;
	public const int layerPlayer = 11;
	public const int layerWorld = 12;

	public const int maskPlayerWorld = (( 1 << 11) | (1 << 12));
	public const int maskEnemyPlayerWorld = (( 1 << 10 ) | ( 1 << 11) | ( 1 << 12 ));

	public static float [] Vector3ToFloatArray(Vector3 v)
	{
		float [] tmp = new float[3];
		tmp[0] = v.x; 
		tmp[1] = v.y;
		tmp[2] = v.z;
		return tmp;
	}

	public static float [] QuatnerionToFloatArray(Quaternion q)
	{
		float [] tmp = new float[4];
		tmp[0] = q.x; 
		tmp[1] = q.y;
		tmp[2] = q.z;
		tmp[3] = q.w;
		return tmp;
	}

	public static Vector3 FloatArrayToVector3(float [] v)
	{
		if(v.Length != 3) return Vector3.zero;
		return new Vector3(v[0],v[1],v[2]);
	}

	public static Quaternion FloatArrayToQuatnerion(float [] q)
	{
		if(q.Length != 4) Quaternion.Euler(Vector3.zero);
		return new Quaternion(q[0],q[1],q[2],q[3]);
	}
}
