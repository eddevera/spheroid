﻿using UnityEngine;
using System.Collections;

// So that pickups/items and weapons are more visible.
// Spawns a light on and changes the material of any object this script is assigned to

// When the player enters the trigger area the light turns off and the material reverts to normal.
// When the player leaves the trigger area the light turns on and the material reverts to the specified alternative.
// If the pickup/item is a of a one-use type, e.g. health, armor, ammo, objectives, the light and this script are deleted.
public class PickupEffects : MonoBehaviour 
{
	[HideInInspector] public Material defaultMaterial;
	[Range(0f,2f)] public float lightIntensity;

	public Material pickupMaterial;

	Light glowLight;
	
	void Start()
	{
		glowLight = gameObject.AddComponent<Light>();
		glowLight.color = pickupMaterial.color;
		defaultMaterial = new Material(gameObject.GetComponentInChildren<MeshRenderer>().material);

		glowLight.range = 5;
		if(lightIntensity != 0)
			glowLight.intensity = lightIntensity;

		gameObject.GetComponentInChildren<MeshRenderer>().material = pickupMaterial;
	}

	void Off()
	{
		glowLight.enabled = false;
		gameObject.GetComponentInChildren<MeshRenderer>().material = defaultMaterial;
	}

	void On()
	{
		glowLight.enabled = true;
		gameObject.GetComponentInChildren<MeshRenderer>().material = pickupMaterial;

	}

	void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.GetComponent<Player>() != null)
			Off ();
	}

	void OnTriggerExit(Collider col)
	{
		if(col.gameObject.GetComponent<Player>() != null)
		{
			if(gameObject.GetComponent<Item>() != null)
			{
				Destroy(gameObject.GetComponent<PickupEffects>().GetComponent<Light>());
				Destroy(gameObject.GetComponent<PickupEffects>());
			}
			On ();
		}
	}
}
