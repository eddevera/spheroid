﻿using UnityEngine;
using System.Collections;

// Base class for actors, track stats. Kill when health = 0.
// Methods can be overriden.

public class Actor : MonoBehaviour, Hittable
{
	public float maxHealth;
	public float maxArmor;

	public float health=100f;
	public float armor;
	public float armorDamageReduction; // This is a percentage.

	public bool constantArmor;
	public bool constantHealth;

	public string actorType;
	[HideInInspector] public bool isAlive;
	[HideInInspector] public bool isArmed;
	[HideInInspector] public Transform currentTransform;

	// We might replace Update child classes so we standarize behavior in ActorUpdate, and make sure to call this in all children.
	void Start()
	{
		StartCoroutine(ActorUpdate());
	}

	// So we can inherit the update function. I'm not sure if we need this, but this seems to work.
	public virtual IEnumerator ActorUpdate()
	{
		// What you would normally put in update goes in this loop.
		while (true)
		{
			currentTransform = gameObject.transform;
			if(isAlive)
				if (health == 0)
			{
				this.isAlive = false;
				this.Kill ();
			}
			yield return new WaitForEndOfFrame();
		}
	}

	public void AddHealth(float value)
	{
		if(!constantHealth)
		{
			health = health + value;
			if(health > maxHealth)
				health = maxHealth;
		}
	}

	public void AddArmor(float value)
	{
		if(!constantArmor)
		{
			armor = armor + value;
			if(armor > maxArmor)
				armor = maxArmor;
		}
	}

	public float getHealth() {
		return health;
	}

	public float getArmor() {
		return armor;
	}

	public void RemoveHealth(float value)
	{
		if(!constantHealth)
		{
			health = health - value;
			if(health < 0)
				health = 0;
		}
	}

	public void RemoveArmor(float value)
	{
		if(!constantArmor)
		{
			armor = armor - value;
			if(armor < 0)
				armor = 0;
		}
	}

	public virtual void Kill()
	{
		Destroy (gameObject);
	}

	public virtual void OnHit(Vector3 dir, int value)
	{
		// Damage dampening.
		if(armor != 0)
		{
			RemoveHealth(value - (value * armorDamageReduction));
			RemoveArmor (value);
		}
		else
			RemoveHealth(value);
	}

	public void PlaySound(AudioSource source, AudioClip clip)
	{
		source.volume *= MusicSingleton.GetInstance().fxVolume;
		source.clip = clip;
		source.Play();
	}

	public IEnumerator MarkForDelete()
	{
		yield return new WaitForSeconds(15);
		GameObject tmp = GameObject.Find("Delete");
		if(tmp != null)
			gameObject.transform.parent = tmp.transform;
	}
}
