﻿using UnityEngine;
using System.Collections;

// For doors.

public class WorldObject : MonoBehaviour 
{
	public string worldObjectType;
	[HideInInspector] public bool active;

	public virtual void Reset()
	{
		//override;
	}

	public virtual void Disable()
	{
		//override;
	}
}
