﻿using UnityEngine;
using System.Collections;

// Base Item class.

public class Item : MonoBehaviour
{
	[HideInInspector] public bool pickedUp = false;
	[HideInInspector] public Player player;

	public string itemType;

	public AudioClip pickupSound;
	public int value;

	void Start()
	{
		// So we don't make a mess of the heirarchy.
		if(GameObject.Find("Items") != null)
			gameObject.transform.parent = GameObject.Find("Items").transform;
	}

	public virtual void OnPickup()
	{
		// Do things.
	}

	public void HandleTouch(Collider col)
	{
		if(!pickedUp)
		{
			player = col.gameObject.GetComponent<Player>();
			if(player != null)
			{
				pickedUp = true;
				OnPickup ();
				SoundSetPlayer.PlaySoundAt(pickupSound,gameObject.transform.position);
				Destroy (gameObject);
			}
		}
	}

	void OnTriggerEnter(Collider col)
	{
		HandleTouch(col);
	}
}
