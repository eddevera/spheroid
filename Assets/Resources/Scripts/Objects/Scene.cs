﻿using UnityEngine;
using System.Collections;

// Pretty much just for level transitions.
// fromLevel specifies whether or not we're coming from a level, this value is used by GameManager in the save handling step.

public class Scene : MonoBehaviour 
{
	[HideInInspector] public string level;
	[HideInInspector] public bool fromLevel;

	void Start () 
	{
		level = Application.loadedLevelName;
		gameObject.transform.parent = GameObject.Find("PlayerPackage").transform;
	}
	
}
