﻿using UnityEngine;
using System.Collections;

// This is just to test ray casting.

public class TestPhys : MonoBehaviour, Hittable
{
	Color oldColor;
	void Start()
	{
		// Save the original color.
		Color tmp = GetComponent<MeshRenderer>().material.color;
		oldColor = new Color(tmp.r,tmp.g,tmp.b,tmp.a);
	}

	public void OnHit(Vector3 hit, int damage)
	{
		gameObject.GetComponent<Rigidbody>().AddForce(hit * 300 * damage);
		GetComponent<MeshRenderer>().material.color = Color.red;
		StartCoroutine(LerpBack(2));
	}
	
	IEnumerator LerpBack(float duration)
	{
		// Transition from red to oldColor in 'duration' seconds.
		float elapsed = 0;
		while(elapsed < duration)
		{
			GetComponent<MeshRenderer>().material.color = Color.Lerp(Color.red,oldColor,elapsed/duration);
			elapsed = elapsed + Time.fixedDeltaTime;
			yield return new WaitForEndOfFrame();
		}
	}
}
