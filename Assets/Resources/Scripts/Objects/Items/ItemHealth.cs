﻿using UnityEngine;
using System.Collections;

// Health variant of Item.

public class ItemHealth : Item
{
	public override void OnPickup()
	{
		player.AddHealth(value);
	}
}
