﻿using UnityEngine;
using System.Collections;

// The base weapon class.
// This base is intended for player weapons, as anything using this class can be equipped/dropped.
// This should not be used for enemy weapons.

public class Weapon : MonoBehaviour 
{
	public string weaponName;		// The weapon's string name.
	public string ammoType;			// The type of ammo the gun needs.
	public int damage; 				// The weapon's damage.
	public Transform muzzleOrigin;	// The weapon's muzzle origin (effects/projectiles/rays originate from here).
	public GameObject projectile;	// What'll be instantiated and fired if we're not using instantDamage.	
	public float recoil;

	public AudioClip pickupSound;	// Sound to play when the weapon is picked up.
	public AudioClip dropSound;		// Sound to play when the weapon is dropped.
	public AudioClip firingSound;	// Sound to play when the weapon is fired.
	public AudioClip emptySound;	// Sound to play when the weapon is fired but the player doesn't have ammo of this type.

	[Range(1f, 300f)] public float roundsPerSecond; // You really don't need to shoot faster than 300 rounds a second.
	public bool automatic;							// Will the weapon continue to fire while you hold down the left mouse button.
	public bool instantDamage;						// Instantaneous damage or uses projectiles.

	public GameObject muzzleEffect;
	MuzzleFlare muzzleFlare;

	[HideInInspector] public Transform currentTransform;

	[HideInInspector] public bool dropped = false;   			// Has the weapon been dropped. Needed so moving the view doesn't equip/unequip the weapon.
	[HideInInspector] public bool firing = false;				// Is the weapon firing currently.
	[HideInInspector] public GameObject player;					// Shorthand reference to player.
	[HideInInspector] public PlayerRaycaster raycaster;			// Shorthand reference to player's raycaster.
	[HideInInspector] public Transform playerCamera;			// Shorthand reference to player's camera.
	[HideInInspector] public PlayerAmmoTracker.AmmoType ammo;	// Instance of AmmoType object of our AmmoType.

	public bool configuring;	// If we're configuring the viewmodel, so we can update these values in real-time.
	[Range(-2f, 2f)] public float viewmodelXOffset;
	[Range(-2f, 2f)] public float viewmodelYOffset;
	[Range(-2f, 2f)] public float viewmodelZOffset;

	// EnemyTracking.cs relies on this variable. 
	// If an equipped weapon is blocking the enemy's view of the player tracking the weapon counts as seeing the player.
	[HideInInspector] public bool pickedUp; // Is the weapon being wielded currently. 

	void Start()
	{
		// So we don't make a mess of the heirarchy.
		if(GameObject.Find("Weapons") != null)
			gameObject.transform.parent = GameObject.Find("Weapons").transform;

		player = GameObject.Find ("Player");
		raycaster = gameObject.GetComponent<PlayerRaycaster>();
		pickedUp = false;

		if(muzzleEffect != null)
		{
			GameObject tmp = Instantiate(muzzleEffect,muzzleOrigin.transform.position, muzzleOrigin.transform.rotation) as GameObject;
			tmp.transform.parent = muzzleOrigin.transform;
			muzzleFlare = gameObject.GetComponentInChildren<MuzzleFlare>();
		}
	}

	public virtual void Shoot()
	{
		PlayerCamera pCam = GameObject.Find("Player").GetComponentInChildren<PlayerCamera>();
		
		// If we have ammo, shoot. Otherwise play the empty sound.
		if(player.GetComponent<PlayerAmmoTracker>().ammoStorage.TryGetValue(ammoType, out ammo))
		{
			if(ammo.count != 0)
			{
				ammo.count--;
				PlaySound(firingSound);
				if(instantDamage)
				{
					InstantDamage();
				}
				else
				{
					Vector3 direction = raycaster.CastRay(0);
					GameObject tmp = Instantiate (projectile,muzzleOrigin.position,new Quaternion()) as GameObject;
					tmp.GetComponent<Projectile>().projectileDamage = damage;
					tmp.GetComponent<Projectile>().direction = direction;
				}
				if(muzzleFlare != null)
					muzzleFlare.Flare();
				pCam.KickView(recoil);
				return;
			}
		}
		PlaySound(emptySound);
	}

	public virtual void InstantDamage()
	{
		raycaster.CastRay(damage);
	}

	public virtual IEnumerator Fire()
	{
		// If we're automatic, fire till either the gun is dropped or till the trigger is released. Else just fire once.
		float fireDelay = 1/roundsPerSecond;
		firing = true;
		if(automatic)
		{
			while(true)
			{
				if(Input.GetMouseButton(0) == false || !pickedUp || dropped || Time.timeScale == 0)
					break;
				Shoot ();
				yield return new WaitForSeconds(fireDelay);
			}
		}
		else
		{
			if(!dropped && Time.timeScale != 0)
			{
				PlaySound(firingSound);
				Shoot ();
				yield return new WaitForSeconds(fireDelay);
			}
		}
		firing = false;
	}

	void OnTriggerEnter(Collider col) // UPDATE THIS CODE TO CHECK FOR PLAYER COMPONENT INSTEAD OF ACTOR.
	{
		// If the weapon is touched by an Actor and that actor is a player, and the player isn't already armed.
		// Then arm the player.
		if(col.gameObject.GetComponent<Player>() != null)
			if(col.gameObject.GetComponent<Actor>().actorType == "player")
				if(!pickedUp && !col.gameObject.GetComponent<Actor>().isArmed)
			{
				BindToPlayer(col.gameObject);
				col.gameObject.GetComponent<Actor>().isArmed = true;
			}
	}
	
	void OnTriggerExit(Collider col) // UPDATE THIS CODE TO CHECK FOR PLAYER COMPONENT INSTEAD OF ACTOR.
	{
		// If the weapon has been dropped, marked the player as disarmed, marked the weapon as not picked up.
		if(dropped)
		{
			if(col.gameObject.GetComponent<Actor>() != null)
				if(col.gameObject.GetComponent<Actor>().actorType == "player")
					if(pickedUp)
				{
					pickedUp = false;
					col.gameObject.GetComponent<Actor>().isArmed = false;
				}
		}
	}

	void Update()
	{
		// If we're picked up, get dropped.
		if(Input.GetKeyDown(KeyCode.G) && pickedUp)
		{
			DropWeapon ();
		}
		// If we're configuring, update the offsets in real-time.
		if(configuring && player != null)
		{
			Transform cameraPos = player.GetComponentInChildren<PlayerCamera>().gameObject.transform;
			gameObject.transform.localPosition = new Vector3(cameraPos.localPosition.x + viewmodelXOffset, 
			                                                 cameraPos.localPosition.y + viewmodelYOffset, 
			                                                 cameraPos.localPosition.z + viewmodelZOffset);
		}
		// If we're not already firing, and we're picked up, and the game isn't paused start shooting.
		if(Input.GetMouseButtonDown(0) && !firing && pickedUp && !(Time.timeScale == 0))
		{
			StartCoroutine(Fire ());
		}
	}

	public void DropWeapon()
	{
		// Drop this weapon.
		dropped = true;
		gameObject.GetComponent<Rigidbody>().useGravity = true;
		gameObject.GetComponent<Collider>().enabled = true;
		gameObject.GetComponent<Rigidbody>().isKinematic = false;
		
		gameObject.transform.parent = GameObject.Find("Weapons").transform;
		PlaySound(dropSound);
	}

	public void BindToPlayer(GameObject _player)
	{
		// Disable the physical properties of the weapon and parent it to the player's camera so it moves with the view.
		player = _player;
		pickedUp = true;
		dropped = false;

		// If the ammo type of this weapon isn't in our ammo tracker add it to the list so it can be tracked.
		if(!player.GetComponent<PlayerAmmoTracker>().ammoStorage.TryGetValue(ammoType, out ammo))
			player.GetComponent<PlayerAmmoTracker>().ammoStorage.Add (ammoType,new PlayerAmmoTracker.AmmoType(ammoType));

		// Disable collisions with the player so the weapon doesn't freak out when dropped.
		gameObject.GetComponent<Collider>().enabled = true; // Band-Aid fix.
		Physics.IgnoreCollision(player.GetComponent<Collider>(),gameObject.GetComponent<Collider>());
		
		gameObject.GetComponent<Rigidbody>().useGravity = false;
		gameObject.GetComponent<Collider>().enabled = false;
		gameObject.GetComponent<Rigidbody>().isKinematic = true;
		
		PlaySound(pickupSound);
		
		playerCamera = player.GetComponentInChildren<PlayerCamera>().gameObject.transform;
		gameObject.transform.parent = playerCamera;
		
		gameObject.transform.localRotation = new Quaternion();
		gameObject.transform.localPosition = new Vector3(playerCamera.localPosition.x + viewmodelXOffset, 
		                                                 playerCamera.localPosition.y + viewmodelYOffset, 
		                                                 playerCamera.localPosition.z + viewmodelZOffset);
	}

	public void PlaySound(AudioClip clip)
	{
		gameObject.GetComponent<AudioSource>().volume = MusicSingleton.GetInstance().fxVolume;
		gameObject.GetComponent<AudioSource>().clip = clip;
		gameObject.GetComponent<AudioSource>().Play();
	}
}
