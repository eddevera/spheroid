﻿using UnityEngine;
using System.Collections;

// If the player has the objective item they may open this wall, otherwise they get the klaxon.

public class StateWall : WorldObject, Triggerable
{
	bool open = false;

	public bool isObjective;

	public AudioClip soundGrant;
	public AudioClip soundDeny;

	public Material grant;
	public Material deny;
	public Material state;

	Light wallLight;

	void Start()
	{
		GetComponent<Renderer>().material = state;
		wallLight = gameObject.AddComponent<Light>();
		wallLight.color = state.color;
		active = true;
		Close ();
	}

	void Update()
	{
		if(active && open)
			Close ();

		if(!active && !open)
			Open();
	}

	void Open()
	{
		open = true;
		gameObject.GetComponent<Collider>().enabled = false;
		gameObject.GetComponent<Renderer>().enabled = false;
	}

	void Close()
	{
		open = false;
		gameObject.GetComponent<Collider>().enabled = true;
		gameObject.GetComponent<Renderer>().enabled = true;
	}

	public void Trigger()
	{
		if(isObjective)
		{
			if(GameObject.Find("Player").GetComponent<Player>().hasObjective)
			{
				StartCoroutine(FlashLight(true));
				SoundSetPlayer.PlaySoundAt(soundGrant,transform.position);
			}
			else
			{
				StartCoroutine(FlashLight(false));
				SoundSetPlayer.PlaySoundAt(soundDeny,transform.position);
			}
		}
	}

	IEnumerator FlashLight(bool access)
	{
		if(access)
		{
			wallLight.color = Color.green;
			GetComponent<Renderer>().material = grant;
		}
		else
		{
			wallLight.color = Color.red;
			GetComponent<Renderer>().material = deny;
		}

		yield return new WaitForSeconds(1);
		GetComponent<Renderer>().material = state;
		wallLight.color = state.color;
		if(access)
		{
			active = false;
			wallLight.enabled = false;
		}
	}
}
